/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    log: function(req,res){
        var usr = req.body.user;
        console.log('value of user:'+usr);
        var pwd = req.body.password;
        Login.findOne({user:usr,password:pwd}).exec((err,data)=>{
            if(err){
                res.send(500,{error:'database error'});
            }
            if(data==null){
                res.view('errlogin',{layout:false});
            }
            res.view('pages/homepage');
        });
    },

    signUp:function(req,res){

        res.view('registration',{layout:false});
    },

    register:function(req,res){
        var usr = req.body.user;
        // console.log('value of user:'+usr);
        // console.log(req['body']);
        var pwd = req.body.password;
        console.log(pwd);
        // console.log('value of password:'+pwd);
        Login.create({user:usr,password:pwd}).exec((err)=>{
           if(err){
              res.send(500,{error:'database error'})
           }
           res.redirect('/homepage');
        //  res.view('pages/homepage');
        });
    }
  

};

