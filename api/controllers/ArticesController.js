/**
 * ArticesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  list: function(req,res){
      Artices.find({}).exec(function(err,articles){
          if(err){
              res.send(500,{error:'database error'});
          }
          res.view('list',{articles:articles});
        
      });
  },

  add: function(req,res){
      res.view('add');
  },

  create: function(req,res){
      var title = req.body.title;
      var body = req.body.body;
      
      Artices.create({title:title,body:body}).exec(function(err){
          if(err){
              res.send(500,{error:'database error'});
          } 
          
          res.redirect('/artices/list');
      });
  },

  delete: function(req,res){
      Artices.destroy({id:req.params.id}).exec(function(err){
          if(err){
              res.send(500,{error:'database error'});
          }

          res.redirect('/artices/list');
      });
  },

  edit: function(req,res){
      Artices.findOne({id:req.params.id}).exec(function(err,article){
          if(err){
              res.send(500,{error:'database error'});
          }

          res.view('edit',{article:article});
      });
  },

  update : function(req,res){
    var title = req.body.title;
    var body = req.body.body;
    
    Artices.update({id:req.params.id},{title:title,body:body}).exec(function(err){
        if(err){
            res.send(500,{error:'database error'});
        } 
        
        res.redirect('/artices/list');
    });
  }

};

