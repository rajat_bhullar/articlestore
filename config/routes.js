/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'login', locals:{layout:false} },
  '/errlogin': { view: 'errlogin', locals:{layout:false} },
  '/homepage':{view: 'pages/homepage'},
  '/artices/list' : 'ArticesController.list',
  '/artices/add' : 'ArticesController.add',
  '/artices/create' : 'ArticesController.create',
  '/artices/delete/:id' : 'ArticesController.delete',
  '/artices/edit/:id' : 'ArticesController.edit',
  '/artices/update/:id' : 'ArticesController.update',
  '/login' : 'LoginController.log',
  '/register' : 'LoginController.register',
  '/signup' : 'LoginController.signUp'


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
